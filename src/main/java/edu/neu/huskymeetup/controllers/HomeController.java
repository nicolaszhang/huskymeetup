package edu.neu.huskymeetup.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by NicolasZHANG on 3/25/15.
 */
@Controller
public class HomeController {
    @RequestMapping(value = "/index")
    public String index() {
        return "index";
    }
}
